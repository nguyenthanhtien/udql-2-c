﻿namespace QuanLyBanHang
{
    partial class frmPhanQuyen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnThemVaiTro = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemNguoiDung = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.trDanhSachVaiTro = new DevExpress.XtraTreeList.TreeList();
            this.TenChucNang = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.Them = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.Xoa = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.Sua = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.Nhap = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.Xuat = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.in_ra = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.trlVaitro = new DevExpress.XtraTreeList.TreeList();
            this.grcVaiTroVaNguoiDung = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.idVaiTro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenVaiTro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MoTa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TrangThai = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.trDanhSachVaiTro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trlVaitro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcVaiTroVaNguoiDung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnThemVaiTro
            // 
            this.btnThemVaiTro.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btnThemVaiTro.Location = new System.Drawing.Point(12, 12);
            this.btnThemVaiTro.Name = "btnThemVaiTro";
            this.btnThemVaiTro.Size = new System.Drawing.Size(113, 23);
            this.btnThemVaiTro.TabIndex = 0;
            this.btnThemVaiTro.Text = "Thêm vai trò";
            this.btnThemVaiTro.Click += new System.EventHandler(this.btnThemVaiTro_Click);
            // 
            // btnThemNguoiDung
            // 
            this.btnThemNguoiDung.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btnThemNguoiDung.Location = new System.Drawing.Point(131, 12);
            this.btnThemNguoiDung.Name = "btnThemNguoiDung";
            this.btnThemNguoiDung.Size = new System.Drawing.Size(114, 23);
            this.btnThemNguoiDung.TabIndex = 1;
            this.btnThemNguoiDung.Text = "Thêm người dùng";
            this.btnThemNguoiDung.Click += new System.EventHandler(this.btnThemNguoiDung_Click);
            // 
            // btnDong
            // 
            this.btnDong.ImageUri.Uri = "Delete;Size16x16;Colored";
            this.btnDong.Location = new System.Drawing.Point(413, 12);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 3;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnXoa.Location = new System.Drawing.Point(332, 12);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 4;
            this.btnXoa.Text = "Xóa";
            // 
            // btnSua
            // 
            this.btnSua.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.btnSua.Location = new System.Drawing.Point(251, 12);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(75, 23);
            this.btnSua.TabIndex = 5;
            this.btnSua.Text = "Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // trDanhSachVaiTro
            // 
            this.trDanhSachVaiTro.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.TenChucNang,
            this.Them,
            this.Xoa,
            this.Sua,
            this.Nhap,
            this.Xuat,
            this.in_ra});
            this.trDanhSachVaiTro.Cursor = System.Windows.Forms.Cursors.Default;
            this.trDanhSachVaiTro.Location = new System.Drawing.Point(224, 253);
            this.trDanhSachVaiTro.Name = "trDanhSachVaiTro";
            this.trDanhSachVaiTro.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckEdit4,
            this.repositoryItemCalcEdit1,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemCheckEdit7});
            this.trDanhSachVaiTro.Size = new System.Drawing.Size(763, 171);
            this.trDanhSachVaiTro.TabIndex = 6;
            // 
            // TenChucNang
            // 
            this.TenChucNang.Caption = "Vai trò";
            this.TenChucNang.FieldName = "TenChucNang";
            this.TenChucNang.Name = "TenChucNang";
            this.TenChucNang.Visible = true;
            this.TenChucNang.VisibleIndex = 0;
            // 
            // Them
            // 
            this.Them.Caption = "Thêm";
            this.Them.ColumnEdit = this.repositoryItemCheckEdit2;
            this.Them.FieldName = "Them";
            this.Them.Name = "Them";
            this.Them.Visible = true;
            this.Them.VisibleIndex = 1;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // Xoa
            // 
            this.Xoa.Caption = "Xóa";
            this.Xoa.ColumnEdit = this.repositoryItemCheckEdit3;
            this.Xoa.FieldName = "Xoa";
            this.Xoa.Name = "Xoa";
            this.Xoa.Visible = true;
            this.Xoa.VisibleIndex = 2;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // Sua
            // 
            this.Sua.Caption = "Sửa";
            this.Sua.ColumnEdit = this.repositoryItemCheckEdit4;
            this.Sua.FieldName = "Sua";
            this.Sua.Name = "Sua";
            this.Sua.Visible = true;
            this.Sua.VisibleIndex = 3;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            // 
            // Nhap
            // 
            this.Nhap.Caption = "Nhập";
            this.Nhap.ColumnEdit = this.repositoryItemCheckEdit7;
            this.Nhap.FieldName = "Nhap";
            this.Nhap.Name = "Nhap";
            this.Nhap.Visible = true;
            this.Nhap.VisibleIndex = 4;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // Xuat
            // 
            this.Xuat.Caption = "Xuất";
            this.Xuat.ColumnEdit = this.repositoryItemCheckEdit5;
            this.Xuat.FieldName = "Xuat";
            this.Xuat.Name = "Xuat";
            this.Xuat.Visible = true;
            this.Xuat.VisibleIndex = 5;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // in_ra
            // 
            this.in_ra.Caption = "In ra";
            this.in_ra.ColumnEdit = this.repositoryItemCheckEdit6;
            this.in_ra.FieldName = "in_ra";
            this.in_ra.Name = "in_ra";
            this.in_ra.Visible = true;
            this.in_ra.VisibleIndex = 6;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // trlVaitro
            // 
            this.trlVaitro.Location = new System.Drawing.Point(14, 41);
            this.trlVaitro.Name = "trlVaitro";
            this.trlVaitro.Size = new System.Drawing.Size(204, 383);
            this.trlVaitro.TabIndex = 7;
            // 
            // grcVaiTroVaNguoiDung
            // 
            this.grcVaiTroVaNguoiDung.Location = new System.Drawing.Point(223, 43);
            this.grcVaiTroVaNguoiDung.MainView = this.gridView1;
            this.grcVaiTroVaNguoiDung.Name = "grcVaiTroVaNguoiDung";
            this.grcVaiTroVaNguoiDung.Size = new System.Drawing.Size(774, 204);
            this.grcVaiTroVaNguoiDung.TabIndex = 8;
            this.grcVaiTroVaNguoiDung.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.idVaiTro,
            this.TenVaiTro,
            this.MoTa,
            this.TrangThai});
            this.gridView1.GridControl = this.grcVaiTroVaNguoiDung;
            this.gridView1.Name = "gridView1";
            // 
            // idVaiTro
            // 
            this.idVaiTro.Caption = "Mã nhóm";
            this.idVaiTro.FieldName = "idVaiTro";
            this.idVaiTro.Name = "idVaiTro";
            this.idVaiTro.Visible = true;
            this.idVaiTro.VisibleIndex = 0;
            // 
            // TenVaiTro
            // 
            this.TenVaiTro.Caption = "Vai trò";
            this.TenVaiTro.FieldName = "TenVaiTro";
            this.TenVaiTro.Name = "TenVaiTro";
            this.TenVaiTro.Visible = true;
            this.TenVaiTro.VisibleIndex = 1;
            // 
            // MoTa
            // 
            this.MoTa.Caption = "Diễn giải";
            this.MoTa.FieldName = "MoTa";
            this.MoTa.Name = "MoTa";
            this.MoTa.Visible = true;
            this.MoTa.VisibleIndex = 2;
            // 
            // TrangThai
            // 
            this.TrangThai.Caption = "Kích hoạt";
            this.TrangThai.FieldName = "TrangThai";
            this.TrangThai.Name = "TrangThai";
            this.TrangThai.Visible = true;
            this.TrangThai.VisibleIndex = 3;
            // 
            // frmPhanQuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 464);
            this.Controls.Add(this.grcVaiTroVaNguoiDung);
            this.Controls.Add(this.trlVaitro);
            this.Controls.Add(this.trDanhSachVaiTro);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnThemNguoiDung);
            this.Controls.Add(this.btnThemVaiTro);
            this.Name = "frmPhanQuyen";
            this.Text = "Phân quyền người dùng";
            this.Load += new System.EventHandler(this.frmPhanQuyen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trDanhSachVaiTro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trlVaitro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcVaiTroVaNguoiDung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnThemVaiTro;
        private DevExpress.XtraEditors.SimpleButton btnThemNguoiDung;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraTreeList.TreeList trDanhSachVaiTro;
        private DevExpress.XtraTreeList.Columns.TreeListColumn TenChucNang;
        private DevExpress.XtraTreeList.TreeList trlVaitro;
        private DevExpress.XtraGrid.GridControl grcVaiTroVaNguoiDung;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn idVaiTro;
        private DevExpress.XtraGrid.Columns.GridColumn TenVaiTro;
        private DevExpress.XtraGrid.Columns.GridColumn MoTa;
        private DevExpress.XtraGrid.Columns.GridColumn TrangThai;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Them;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Xoa;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Sua;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Nhap;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Xuat;
        private DevExpress.XtraTreeList.Columns.TreeListColumn in_ra;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
    }
}