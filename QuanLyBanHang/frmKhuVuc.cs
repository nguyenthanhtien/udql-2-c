﻿using QuanLyBanHang;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACK_ware2
{
    public partial class frmKhuVuc : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmKhuVuc()
        {
            InitializeComponent();
           
        }

        private void frmKhuVuc_Load(object sender, EventArgs e)
        {
           
            gckhuvuc.DataSource = dbe.KhuVucs.ToList();
        }

        private void btn_Load_Click(object sender, EventArgs e)
        {
          
            gckhuvuc.DataSource = dbe.KhuVucs.ToList();
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            var form = new frmUpdateKhuVuc();
            form.Show();
        }

        private void btn_Sua_Click(object sender, EventArgs e)
        {
            //string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_KhuVuc").ToString();
            string id = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_KhuVuc").ToString();
            string ten = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenKhuVuc").ToString();
            string trangthai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TrangThai").ToString();
            var form = new frmEditKhuVuc(id,ten,trangthai);
            form.Show();
        }

        private void btn_Xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_KhuVuc").ToString();
                int id = int.Parse(ma);
                KhuVuc kv = dbe.KhuVucs.Single(n => n.Id_KhuVuc == id);
                dbe.KhuVucs.Remove(kv);
                dbe.SaveChanges();
                gckhuvuc.DataSource = dbe.KhuVucs.ToList();
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
