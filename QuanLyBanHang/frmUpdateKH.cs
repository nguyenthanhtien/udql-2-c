﻿using DACK_ware2;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmUpdateKH : Form
    {
        KhachHang kh  = new KhachHang();
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmUpdateKH()
        {
            InitializeComponent();
            LoadComboBox();
        }

        private void frmUpdateKH_Load(object sender, EventArgs e)
        {
            
        }
        private void LoadComboBox()
        {
            var a = dbe.KhuVucs.ToList();
            lue_KV.Properties.DataSource = a;
            lue_KV.Properties.DisplayMember = "TenKhuVuc";
            lue_KV.Properties.ValueMember = "Id_KhuVuc";
        

        }


        private void btn__CapNhat_Click(object sender, EventArgs e)
        {
            string m = lue_KV.EditValue.ToString();
            kh.Id_KhuVuc = int.Parse(m);
            kh.TenKhachHang = txt_tenkhachhang.Text;
            kh.DiaChi = txt_DiaChi.Text;
            kh.SĐT = txt_SDT.Text;
            kh.Email = txtEmail.Text;
            kh.Website = txt_Website.Text;
            kh.MaSoThue = Int32.Parse(txt_IDThue.Text);
            kh.SoTK = Int32.Parse(txt_sotk.Text);
            kh.TenNganHang = txt_TenNganHang.Text;
            if (cbQuanLy.Checked == true)
            { kh.TrangThai = true; }
            else
            { kh.TrangThai = false; }
            dbe.KhachHangs.Add(kh);
            dbe.SaveChanges();
            MessageBox.Show("Thêm thành công !!");
            this.Close();
        }
    }
}
