﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThayDoiMatKhau : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public string username; 
        public frmThayDoiMatKhau()
        {
            InitializeComponent();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var user = db.NguoiDungs.FirstOrDefault(a => a.Username.Equals(sttTenNguoiDung.Text));
            if (user.Password == txtMatKhauCu.Text)
            {
                if (txtMatKhauMoi.Text != "")
                {
                    if (txtNhapLaiMatKhau.Text != txtMatKhauMoi.Text)
                    {
                        MessageBox.Show("Mật khâu nhập lại không chính xác!");
                    }
                    else
                    {
                        user.Password = txtMatKhauMoi.Text;
                        db.SaveChangesAsync();
                        MessageBox.Show("Đã đổi thành công.");
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Mật khâu không được rỗng!");
                }
              
            }
            else
            {
                MessageBox.Show("Mật khẩu cũ không chính xác!");
            }
        }

        private void frmThayDoiMatKhau_Load(object sender, EventArgs e)
        {
            sttTenNguoiDung.Text = username;
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }
    }
}
