﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;



namespace QuanLyBanHang
{
    public partial class frmSaoLuu : Form
    {

        private SqlConnection conn;
        private SqlDataReader datareader;
        private SqlCommand command;
        string sql;
        string connectionstring = "";
        public frmSaoLuu()
        {
            InitializeComponent();
        }

        private void frmSaoLuu_Load(object sender, EventArgs e)
        {

        }

        private void btnKetnoi_Click(object sender, EventArgs e)
        {
            try
            {
                connectionstring = "Data Source =" + txtServername.Text +"; User Id=" + txtID.Text +"; Password="+txtPW.Text+"";
                conn = new SqlConnection(connectionstring);
                conn.Open();
                //sql = "EXEC QuanLyBanHang";
                sql = "SELECT * FROM sys.databases d WHERE d.database_id>4";
                command = new SqlCommand(sql,conn);
                datareader = command.ExecuteReader();
                cbDatabase.Items.Clear();
                while (datareader.Read())
                {
                    cbDatabase.Items.Add(datareader[0].ToString());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            txtServername.Enabled = false;
            txtID.Enabled = false;
            txtPW.Enabled = false;


        }

        private void btnNgatKN_Click(object sender, EventArgs e)
        {

        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            try
            {
                if(cbDatabase.Text.CompareTo("") == 0)
                {
                    MessageBox.Show("Hãy chọn database !");
                }
                conn = new SqlConnection(connectionstring);
                conn.Open();
                sql = "BACKUP DATABASE " + cbDatabase.Text +" TO DISK = '" +txtPath.Text + "" +cbDatabase.Text+"-" + DateTime.Now.Ticks.ToString()+".bak'" ;
                command = new SqlCommand(sql, conn);
                command.ExecuteNonQuery();
                MessageBox.Show("Sao lưu cơ sở dữ liệu thành công !");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
