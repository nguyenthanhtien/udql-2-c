﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmSuaDonViTinh : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmSuaDonViTinh(string id, string ten, string ghichu, string trangthai)
        {
            InitializeComponent();
            Loaddata(id, ten, ghichu, trangthai);
        }

      private void Loaddata(string id, string ten, string ghichu, string trangthai)
        {
            txtMa.Text = id;
            txtTen.Text = ten;
            txtGhiChu.Text = trangthai;

        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txtMa.Text);
            DonViTinh donvitinh = dbe.DonViTinhs.Single(n => n.Id_DV == id);
            donvitinh.TenDonVi= txtTen.Text;
            donvitinh.GhiChu = txtGhiChu.Text;
            if (cbConQuanLy.Checked == true)
            { donvitinh.TrangThai = true; }
            else { donvitinh.TrangThai = false; }
           
            dbe.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
        }
    }
}
