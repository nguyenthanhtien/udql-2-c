﻿using DACK_ware2;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmUpdateKhuVuc : Form
    {
        KhuVuc kv = new KhuVuc();
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmUpdateKhuVuc()
        {
            InitializeComponent();
           
        }

        private void frmUpdateKhuVuc_Load(object sender, EventArgs e)
        {
            
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            kv.TenKhuVuc = txt_TenKV.Text;
            if (cbQuanLy.Checked == true)
            {
                kv.TrangThai = true;
            }
            else
            {
                kv.TrangThai = false;
            }
          //  kv.TrangThai = Int32.Parse(txt_tt.Text);
            dbe.KhuVucs.Add(kv);
            dbe.SaveChanges();
            MessageBox.Show("Thêm thành công !!");
            this.Close();
        }
    }
}
