﻿namespace QuanLyBanHang
{
    partial class frmNhatKyHeThong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcNhatKyHeThong = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcNhatKyHeThong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcNhatKyHeThong
            // 
            this.gcNhatKyHeThong.Location = new System.Drawing.Point(6, 10);
            this.gcNhatKyHeThong.MainView = this.gridView1;
            this.gcNhatKyHeThong.Name = "gcNhatKyHeThong";
            this.gcNhatKyHeThong.Size = new System.Drawing.Size(646, 317);
            this.gcNhatKyHeThong.TabIndex = 0;
            this.gcNhatKyHeThong.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcNhatKyHeThong;
            this.gridView1.Name = "gridView1";
            // 
            // frmNhatKyHeThong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 326);
            this.Controls.Add(this.gcNhatKyHeThong);
            this.Name = "frmNhatKyHeThong";
            this.Text = "frmNhatKyHeThong";
            this.Load += new System.EventHandler(this.frmNhatKyHeThong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcNhatKyHeThong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcNhatKyHeThong;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    }
}