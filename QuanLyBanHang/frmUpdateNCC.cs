﻿using DACK_ware2;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
     
    public partial class frmUpdateNCC : Form
    {
        NhaCungCap ncc = new NhaCungCap();
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();

        public frmUpdateNCC()
        {
            InitializeComponent();
            LoadComboBox();
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            string m = lue_KV.EditValue.ToString();
            ncc.id_KV = int.Parse(m);
            ncc.TenNCC = txt_TenNCC.Text;
            ncc.SDT = txt_SDT.Text;
            ncc.ChucVu = txt_ChucVu.Text;
            ncc.DiaChi = txt_DiaChi.Text;
            if (CbQuanLy.Checked == true)
            { ncc.TrangThai = true; }
            else
            {
                ncc.TrangThai = false;
            }
            dbe.NhaCungCaps.Add(ncc);
            dbe.SaveChanges();
            MessageBox.Show("Thêm thành công !!");
            this.Close();
        }

        private void frmUpdateNCC_Load(object sender, EventArgs e)
        {

        }
        private void LoadComboBox()
        {
            var a = dbe.KhuVucs.ToList();
            lue_KV.Properties.DataSource = a.ToList();
            lue_KV.Properties.DisplayMember = "TenKhuVuc";
            lue_KV.Properties.ValueMember = "Id_KhuVuc";
            

        }
    }
}
